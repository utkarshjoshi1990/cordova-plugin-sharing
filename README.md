---
title: Sharing data
description: share data with other apps using your Keychain.
---

# cordova-plugin-sharing

This Plugin is used to share data with other apps using your Keychain group.

Note :- make sure you have created & configured group settings with your projects.

it is not available until after the `deviceready` event.

## Examples

## Set JSON Object into your keychain group

```js
cordova.plugins.Sharing.setJSONObj(
  "your group Name",
  "your key",
  "your value as object",
  success,
  error
);

function success(res) {
  console.log("data ", res);
}

function error(err) {
  console.log("error ", err);
}
```

## Installation

    cordova plugin add https://utkarshjoshi1990@bitbucket.org/utkarshjoshi1990/cordova-plugin-sharing.git

### Supported Platforms

- iOS
