/********* Sharing.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface Sharing : CDVPlugin {
  // Member variables go here.
}

- (void)setJSONObj:(CDVInvokedUrlCommand*)command;
- (void)getJSONObj:(CDVInvokedUrlCommand*)command;
- (void)removeJSONObj : (CDVInvokedUrlCommand*)command;
@end

@implementation Sharing

- (void)setJSONObj:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* keyChainGroup = [command.arguments objectAtIndex:0];
    NSString* key = [command.arguments objectAtIndex:1];
    NSArray* userInfo = [command.arguments objectAtIndex:2];
    NSUserDefaults *myDefaults = [[NSUserDefaults alloc]
                                  initWithSuiteName:keyChainGroup];
    [myDefaults setObject:userInfo forKey:key];
    if (key != nil && [key length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"gotKey"];
    } else if (userInfo != nil && [userInfo count] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"gotValue"];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
- (void)getJSONObj:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* keyChainGroup = [command.arguments objectAtIndex:0];
    NSString* key = [command.arguments objectAtIndex:1];
    NSUserDefaults *getDefaults = [[NSUserDefaults alloc]
                                  initWithSuiteName:keyChainGroup];
     if (key != nil && [key length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"gotKey"];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }                              
    NSArray* data = [getDefaults objectForKey:key];
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:data];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
- (void)removeJSONObj : (CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* keyChainGroup = [command.arguments objectAtIndex:0];
    NSString* key = [command.arguments objectAtIndex:1];
    NSUserDefaults *myDefaults = [[NSUserDefaults alloc]
                                  initWithSuiteName:keyChainGroup];
    [myDefaults removeObjectForKey:key];
    if (key != nil && [key length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"gotKey"];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
